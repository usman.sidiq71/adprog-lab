package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0.0f;
    private float minTemp = 0.0f;
    private float tempSum = 0.0f;
    private int numReadings = 2;

    public StatisticsDisplay(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData) o;
            if(weatherData.getTemperature() > maxTemp){
                maxTemp = weatherData.getTemperature();
            }
            if(weatherData.getTemperature() < minTemp){
                minTemp = weatherData.getTemperature();
            }
            if(maxTemp == 0){
                maxTemp = minTemp;
            }
            else if(minTemp == 0){
                minTemp = maxTemp;
            }
            tempSum = maxTemp + minTemp;
            display();

        }
    }
}
