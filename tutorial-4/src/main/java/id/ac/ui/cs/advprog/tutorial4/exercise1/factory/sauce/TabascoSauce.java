package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class TabascoSauce implements Sauce {
    public String toString() {
        return "Tabasco Sauce";
    }
}
