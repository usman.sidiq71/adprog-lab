package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ThiccCrustDough implements Dough {
    public String toString() {
        return "ThiccCrust style thicc crust with extra meme dough";
    }
}
