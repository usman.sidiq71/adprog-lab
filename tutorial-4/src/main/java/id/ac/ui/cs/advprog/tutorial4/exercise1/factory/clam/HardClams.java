package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class HardClams implements Clams {
    public String toString() {
        return "Hard Clams from Celebes Island";
    }
}
