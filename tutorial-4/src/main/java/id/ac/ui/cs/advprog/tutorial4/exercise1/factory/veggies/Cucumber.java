package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Cucumber implements Veggies {

    public String toString() {
        return "They are using Cucumber";
    }
}
