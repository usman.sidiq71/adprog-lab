package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
    private static Singleton awesomeInstance;

    // What's missing in this Singleton declaration?
    private Singleton() {}

    public static Singleton getInstance() {
        if (awesomeInstance == null) {
            awesomeInstance = new Singleton();
        }
        return awesomeInstance;
    }
}
