package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RedPepperTest {
    private RedPepper redPepper;

    @Before
    public void setUp() {
        redPepper = new RedPepper();
    }

    @Test
    public void testReturn() {
        assertEquals("Red Pepper", redPepper.toString());
    }
}
