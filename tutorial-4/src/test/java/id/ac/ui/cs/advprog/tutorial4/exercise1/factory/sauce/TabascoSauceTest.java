package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TabascoSauceTest {
    private TabascoSauce tabascoSauce;

    @Before
    public void setUp() {
        tabascoSauce = new TabascoSauce();
    }

    @Test
    public void testReturn() {
        assertEquals("Tabasco Sauce", tabascoSauce.toString());
    }
}
