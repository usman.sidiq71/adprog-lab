package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MushroomTest {
    private Mushroom mushroom;

    @Before
    public void setUp() {
        mushroom = new Mushroom();
    }

    @Test
    public void testReturn() {
        assertEquals("Mushrooms", mushroom.toString());
    }
}
