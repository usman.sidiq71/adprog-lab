package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FrozenClamTest {
    private FrozenClams frozenClams;

    @Before
    public void setUp() {
        frozenClams = new FrozenClams();
    }

    @Test
    public void testReturn() {
        assertEquals("Frozen Clams from Chesapeake Bay", frozenClams.toString());
    }

}
