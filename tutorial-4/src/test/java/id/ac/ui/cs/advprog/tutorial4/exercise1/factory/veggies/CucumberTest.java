package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CucumberTest {
    private Cucumber cucumber;

    @Before
    public void setUp() {
        cucumber = new Cucumber();
    }

    @Test
    public void testReturn() {
        assertEquals("They are using Cucumber", cucumber.toString());
    }
}
