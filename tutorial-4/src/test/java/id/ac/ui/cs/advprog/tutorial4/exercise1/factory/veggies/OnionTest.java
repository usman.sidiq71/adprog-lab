package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OnionTest {
    private Onion onion;

    @Before
    public void setUp() {
        onion = new Onion();
    }

    @Test
    public void testReturn() {
        assertEquals("Onion", onion.toString());
    }
}
