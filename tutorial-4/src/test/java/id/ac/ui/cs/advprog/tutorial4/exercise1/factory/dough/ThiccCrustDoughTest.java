package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThiccCrustDoughTest {
    private ThiccCrustDough thiccCrustDough;

    @Before
    public void setUp() {
        thiccCrustDough = new ThiccCrustDough();
    }

    @Test
    public void testReturn() {
        assertEquals("ThiccCrust style thicc crust with extra meme dough", thiccCrustDough.toString());
    }
}
