package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees{
    public FrontendProgrammer(String name, double salary) {
        this.name = name;
        this.role = "Front End Programmer";
        if(salary < 30000.00){
            throw new IllegalArgumentException();
        }
        this.salary = salary;
        //TODO Implement
    }

    @Override
    public double getSalary() {
        return salary;
        //TODO Implement
    }
}
