package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees{
    public SecurityExpert(String name, double salary) {
        this.name = name;
        this.role = "Security Expert";
        if(salary < 70000.00){
            throw new IllegalArgumentException();
        }
        this.salary = salary;
        //TODO Implement
    }

    @Override
    public double getSalary() {
        return salary;
        //TODO Implement
    }
}
