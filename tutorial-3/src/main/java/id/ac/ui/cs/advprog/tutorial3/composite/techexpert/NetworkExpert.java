package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees{
    public NetworkExpert(String name, double salary) {
        this.name = name;
        this.role = "Network Expert";
        if(salary < 50000.00){
            throw new IllegalArgumentException();
        }
        this.salary = salary;
        //TODO Implement
    }

    @Override
    public double getSalary() {
        return salary;
        //TODO Implement
    }
}
