package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees{
    public Cto(String name, double salary) {
        this.name = name;
        this.role = "CTO";
        if(salary < 100000.00){
            throw new IllegalArgumentException();
        }
        this.salary = salary;
        //TODO Implement
    }

    @Override
    public double getSalary() {
        return salary;
        //TODO Implement
    }
}
