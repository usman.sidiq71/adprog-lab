package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        employeesList.add(employees);
        //TODO Implement
    }

    public double getNetSalaries() {
        int netSalaries = 0;
        for(int i = 0; i < employeesList.size(); i++){
            netSalaries += employeesList.get(i).getSalary();
        }
        return netSalaries;
        //TODO Implement
    }

    public List<Employees> getAllEmployees() {
        return employeesList;
        //TODO Impleme
    }
}
